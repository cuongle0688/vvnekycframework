
Pod::Spec.new do |spec|
  spec.platform = :ios
  spec.ios.deployment_target = '9.0'
  spec.name         = "VVNEkycFramework"
  spec.version      = "0.1.3"
  spec.summary      = "VVNEkycFramework"
  spec.requires_arc = true
  
  spec.homepage     = "https://bitbucket.org/cuongle0688/vvnekycframework"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author             = { "Le van Cuong" => "cuongle0688@gmail.com" }

  spec.source       = { :git => "https://cuongle0688@bitbucket.org/cuongle0688/vvnekycframework.git", :tag => "#{spec.version}" }
  spec.static_framework = true

  spec.framework = "UIKit"
  spec.dependency 'Alamofire', '~> 4.9.1'
  spec.dependency 'GoogleMobileVision/FaceDetector', '~> 1.6.0'

  spec.source_files  = "VVNEkycFramework/**/*.swift"
  spec.resources = "VVNEkycFramework/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"
  spec.swift_version = "5.0"

end

//
//  TakePhotoViewController.swift
//  AntiFakeFaceSwift
//
//  Created by Le van Cuong on 6/10/19.
//  Copyright © 2019 Le van Cuong. All rights reserved.
//

import UIKit
import AVFoundation
@available(iOS 10.0, *)
class TakePhotoViewController: BaseViewController, AVCapturePhotoCaptureDelegate, TakePhotoResultViewControllerDelegate {
    var stillImageOutput: AVCapturePhotoOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    var frontIDCard: UIImage?
    var backIDCard: UIImage?
    @IBOutlet weak var labelHeader: UILabel!
    @IBOutlet weak var previewView: UIView!
    var isClickCapture:Bool = false
    var captureImage:UIImage?
    var isFrontCard:Bool = true
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isFrontCard = true
        if self.captureSession == nil {
            captureSession = AVCaptureSession()
        }
        captureSession!.sessionPreset = .photo
        guard let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
            else {
                print("Unable to access back camera!")
                return
        }
        do {
            let input = try AVCaptureDeviceInput(device: backCamera)
            stillImageOutput = AVCapturePhotoOutput()
            if captureSession!.canAddInput(input) && captureSession!.canAddOutput(stillImageOutput) {
                captureSession!.addInput(input)
                captureSession!.addOutput(stillImageOutput)
                setupLivePreview()
            }
        }
        catch let error  {
            print("Error Unable to initialize back camera:  \(error.localizedDescription)")
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isClickCapture = false
        if !self.captureSession!.isRunning {
            captureSession = AVCaptureSession()
            captureSession!.sessionPreset = .photo
            guard let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
                else {
                    print("Unable to access back camera!")
                    return
            }
            do {
                let input = try AVCaptureDeviceInput(device: backCamera)
                stillImageOutput = AVCapturePhotoOutput()
                if captureSession!.canAddInput(input) && captureSession!.canAddOutput(stillImageOutput) {
                    captureSession!.addInput(input)
                    captureSession!.addOutput(stillImageOutput)
                    setupLivePreview()
                }
            }
            catch let error  {
                print("Error Unable to initialize back camera:  \(error.localizedDescription)")
            }
        }
        if(self.isFrontCard){
            self.labelHeader.text = "Mặt trước\nChứng minh thư/Thẻ căn cước"
        }else{
            self.labelHeader.text = "Mặt sau\nChứng minh thư/Thẻ căn cước"
        }
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.captureSession.stopRunning()
    }
    func setupLivePreview() {
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
        
        videoPreviewLayer.videoGravity = .resizeAspectFill
        videoPreviewLayer.connection?.videoOrientation = .portrait
        previewView.layer.addSublayer(videoPreviewLayer)
        
        DispatchQueue.global(qos: .userInitiated).async { //[weak self] in
            self.captureSession!.startRunning()
            DispatchQueue.main.async {
                self.videoPreviewLayer.frame = self.previewView.bounds
            }
        }
    }
    @IBAction func clickCapture(_ sender: Any) {
        if #available(iOS 11.0, *) {
            let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
            stillImageOutput.capturePhoto(with: settings, delegate: self)
        } else {
            let settings = AVCapturePhotoSettings.init()
            stillImageOutput.capturePhoto(with: settings, delegate: self)
        }
    }
    @available(iOS 11.0, *)
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        guard let imageData = photo.fileDataRepresentation()
            else { return }
        let image = UIImage(data: imageData)
        if (!isClickCapture){
            self.captureImage = image
            let podBundle = Bundle(for: TakePhotoViewController.self)
            let bundleURL = podBundle.url(forResource: "VVNEkyc", withExtension: "bundle")
            let bundle = Bundle(url: bundleURL!)!
            let storyBoard = UIStoryboard.init(name: "VVNMain", bundle: bundle)
            let resultView = storyBoard.instantiateViewController(withIdentifier: "TakePhotoResultViewController") as! TakePhotoResultViewController
            resultView.image = image
            resultView.textDescription = self.labelHeader.text
            resultView.delegate = self
            self.present(resultView, animated: true, completion: nil)
            self.isClickCapture = true
        }
    }
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        if (photoSampleBuffer) != nil {
            let data = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: photoSampleBuffer!, previewPhotoSampleBuffer: previewPhotoSampleBuffer)
            let image = UIImage.init(data: data!)
            if (!isClickCapture){
                self.captureImage = image
                let podBundle = Bundle(for: TakePhotoViewController.self)
                let bundleURL = podBundle.url(forResource: "VVNEkyc", withExtension: "bundle")
                let bundle = Bundle(url: bundleURL!)!
                let storyBoard = UIStoryboard.init(name: "VVNMain", bundle: bundle)
                let resultView = storyBoard.instantiateViewController(withIdentifier: "TakePhotoResultViewController") as! TakePhotoResultViewController
                resultView.image = image
                resultView.textDescription = self.labelHeader.text
                resultView.delegate = self
                self.present(resultView, animated: true, completion: nil)
                self.isClickCapture = true
            }
        }
    }
    func clickBtnConfirm() {
        if(self.isFrontCard){
            self.isFrontCard = false
            self.labelHeader.text = "Mặt sau\nChứng minh thư/Thẻ căn cước"
            frontIDCard = self.resize(image: captureImage!)
        }else{
            backIDCard = self.resize(image: captureImage!)
            let podBundle = Bundle(for: TakePhotoViewController.self)
            let bundleURL = podBundle.url(forResource: "VVNEkyc", withExtension: "bundle")
            let bundle = Bundle(url: bundleURL!)!
            let storyBoard = UIStoryboard.init(name: "VVNMain", bundle: bundle)
            let camera = storyBoard.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
            self.navigationController?.pushViewController(camera, animated: true)
            self.isFrontCard = true;
            self.captureSession!.stopRunning()
        }
    }
}

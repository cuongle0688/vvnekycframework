//
//  TakePhotoResultViewController.swift
//  AntiFakeFaceSwift
//
//  Created by Le van Cuong on 6/10/19.
//  Copyright © 2019 Le van Cuong. All rights reserved.
//

import UIKit
protocol TakePhotoResultViewControllerDelegate {
    func clickBtnConfirm()
}
class TakePhotoResultViewController: BaseViewController {
    var delegate:TakePhotoResultViewControllerDelegate?
    var image:UIImage?
    var textDescription:String?
    
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var btnConfirm: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnConfirm.layer.cornerRadius = self.btnConfirm.frame.size.height/2
        self.btnConfirm.clipsToBounds = true
        self.imageView.image = self.image;
        self.labelDescription.text = self.textDescription;
    }
    
    @IBAction func clickBtnConfirm(_ sender: Any) {
        self.delegate?.clickBtnConfirm()
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func clickBtnBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}

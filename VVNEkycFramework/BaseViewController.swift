//
//  BaseViewController.swift
//  AntiFakeFaceSwift
//
//  Created by Le van Cuong on 6/10/19.
//  Copyright © 2019 Le van Cuong. All rights reserved.
//

import UIKit
import AVFoundation

public class BaseViewController: UIViewController {
    var captureSession: AVCaptureSession?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
    }
    func showAlert(message:String){
        let alert = UIAlertController.init(title: "ERROR", message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "OK", style: .default) { (alert) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    func showAlertInfo(message:String){
        let alert = UIAlertController.init(title: "INFO", message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "OK", style: .default) { (alert) in
        }
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    func imageFromSampleBuffer(sampleBuffer:CMSampleBuffer) -> UIImage{
        let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
        CVPixelBufferLockBaseAddress(imageBuffer!, [])
        let baseAddress = CVPixelBufferGetBaseAddress(imageBuffer!)
        let bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer!)
        let width = CVPixelBufferGetWidth(imageBuffer!)
        let height = CVPixelBufferGetHeight(imageBuffer!)
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageByteOrderInfo.order32Little.rawValue | CGImageAlphaInfo.premultipliedFirst.rawValue)
        let context = CGContext(data: baseAddress, width: width, height: height, bitsPerComponent: 8, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
        let quartzImage = context!.makeImage();
        CVPixelBufferUnlockBaseAddress(imageBuffer!,[]);
        // Create an image object from the Quartz image
        let image = UIImage.init(cgImage: quartzImage!, scale: 1.0, orientation: .right)
        return image;
    }

    func resize(image: UIImage, maxHeight: Float = 1024, maxWidth: Float = 800) -> UIImage? {
        var actualHeight: Float = Float(image.size.height)
        var actualWidth: Float = Float(image.size.width)
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in:rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img
    }
}

//
//  TakePhotoForIos9ViewController.swift
//  AntiFakeFaceSwift
//
//  Created by Le van Cuong on 6/12/19.
//  Copyright © 2019 Le van Cuong. All rights reserved.
//

import UIKit
import AVFoundation
public enum IdentifyType {
    case front
    case back
}
public protocol TakePhotoDelegate {
    func getIDImageResult(imageIdentify:UIImage?, type: IdentifyType)
}
public class TakePhotoForIos9ViewController: BaseViewController,  TakePhotoResultViewControllerDelegate {
    var stillImageOutput: AVCaptureStillImageOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    public var delegate: TakePhotoDelegate?
    var idCard: UIImage?
    @IBOutlet weak var labelHeader: UILabel!
    @IBOutlet weak var previewView: UIView!
    var isClickCapture:Bool = false
    var captureImage:UIImage?
    public var idType: IdentifyType = .front
    override public func viewDidLoad() {
        super.viewDidLoad()
        if self.captureSession == nil {
            captureSession = AVCaptureSession()
        }
        captureSession!.sessionPreset = .photo
        guard let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
            else {
                print("Unable to access back camera!")
                return
        }
        do {
            let input = try AVCaptureDeviceInput(device: backCamera)
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput?.outputSettings = [AVVideoCodecKey : AVVideoCodecJPEG]
            if captureSession!.canAddInput(input) && captureSession!.canAddOutput(stillImageOutput) {
                captureSession!.addInput(input)
                captureSession!.addOutput(stillImageOutput)
                setupLivePreview()
            }
        }
        catch let error  {
            print("Error Unable to initialize back camera:  \(error.localizedDescription)")
        }
    }
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isClickCapture = false
        if self.captureSession == nil {
            return
        }
        if !self.captureSession!.isRunning {
            captureSession = AVCaptureSession()
            captureSession!.sessionPreset = .photo
            guard let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
                else {
                    print("Unable to access back camera!")
                    return
            }
            do {
                let input = try AVCaptureDeviceInput(device: backCamera)
                stillImageOutput = AVCaptureStillImageOutput()
                stillImageOutput?.outputSettings = [AVVideoCodecKey : AVVideoCodecJPEG]
                if captureSession!.canAddInput(input) && captureSession!.canAddOutput(stillImageOutput) {
                    captureSession!.addInput(input)
                    captureSession!.addOutput(stillImageOutput)
                    setupLivePreview()
                }
            }
            catch let error  {
                print("Error Unable to initialize back camera:  \(error.localizedDescription)")
            }
        }
        if(self.idType == .front){
            self.labelHeader.text = "Mặt trước\nChứng minh thư/Thẻ căn cước"
        }else{
            self.labelHeader.text = "Mặt sau\nChứng minh thư/Thẻ căn cước"
        }
        self.navigationController?.isNavigationBarHidden = true
    }
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.captureSession.stopRunning()
    }
    func setupLivePreview() {
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
        
        videoPreviewLayer.videoGravity = .resizeAspectFill
        videoPreviewLayer.connection?.videoOrientation = .portrait
        previewView.layer.addSublayer(videoPreviewLayer)
        
        DispatchQueue.global(qos: .userInitiated).async { //[weak self] in
            self.captureSession!.startRunning()
            DispatchQueue.main.async {
                self.videoPreviewLayer.frame = self.previewView.bounds
            }
        }
    }
    @IBAction func clickCapture(_ sender: Any) {
        if let videoConnection = stillImageOutput.connection(with: AVMediaType.video) {
            stillImageOutput.captureStillImageAsynchronously(from: videoConnection) {
                (imageDataSampleBuffer, error) -> Void in
                let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer!)
                let image = UIImage(data: imageData!)
                if (!self.isClickCapture){
                    self.captureImage = image
                    let podBundle = Bundle(for: TakePhotoForIos9ViewController.self)
                    let bundleURL = podBundle.url(forResource: "VVNEkyc", withExtension: "bundle")
                    let bundle = Bundle(url: bundleURL!)!
                    let storyBoard = UIStoryboard.init(name: "VVNMain", bundle: bundle)
                    let resultView = storyBoard.instantiateViewController(withIdentifier: "TakePhotoResultViewController") as! TakePhotoResultViewController
                    resultView.image = image
                    resultView.textDescription = self.labelHeader.text
                    resultView.delegate = self
                    self.present(resultView, animated: true, completion: nil)
                    self.isClickCapture = true
                }
            }
        }
    }

    func clickBtnConfirm() {
        self.delegate?.getIDImageResult(imageIdentify: self.resize(image: captureImage!), type: self.idType)
        self.captureSession!.stopRunning()
    }
}


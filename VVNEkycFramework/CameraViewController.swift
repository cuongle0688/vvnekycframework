//
//  CameraViewController.swift
//  AntiFakeFaceSwift
//
//  Created by Le van Cuong on 6/10/19.
//  Copyright © 2019 Le van Cuong. All rights reserved.
//

import UIKit
import AVFoundation
import GoogleMobileVision
public protocol CameraViewControllerDelegate {
    func getFaceImageResult(imageFace:UIImage?)
}
public class CameraViewController: BaseViewController, AVCaptureVideoDataOutputSampleBufferDelegate {
    var faceDetector:GMVDetector?
    @IBOutlet weak var placeHolder:UIView!
    @IBOutlet weak var overlayView:UIView!
    @IBOutlet weak var imgViewHead:UIImageView!
    @IBOutlet weak var imgViewBackground:UIImageView!
    @IBOutlet weak var imgViewAction:UIImageView!
    @IBOutlet weak var labelAction:UILabel!
    public var delegate: CameraViewControllerDelegate?
    // Video objects.
    var videoDataOutput:AVCaptureVideoDataOutput?
    var videoDataOutputQueue:DispatchQueue?
    var previewLayer:AVCaptureVideoPreviewLayer?
    var lastKnownDeviceOrientation:UIDeviceOrientation = UIDeviceOrientation.unknown
    var isUseFrontCamera:Bool?
    var imageProcess:UIImage?
    var imageFace:UIImage?
    var listAction:[Any]?
    var step = 0
    var buffer:CMSampleBuffer?
    var isChangeToResultView:Bool = false
    let numCountDown = 10
    var countDown = 0
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.countDown = self.numCountDown
        self.videoDataOutputQueue = DispatchQueue(label: "VideoDataOutputQueue")
        self.createListAction()
        self.isUseFrontCamera = true
        self.imgViewHead.isHidden = false
        self.imgViewAction.isHidden = false
        self.labelAction.isHidden = false
        self.imgViewBackground.isHidden = false
        self.isChangeToResultView = false
        
        // Set up default camera settings.
        if self.captureSession == nil {
            self.captureSession = AVCaptureSession.init()
        }
        self.captureSession!.sessionPreset = .medium;
        self.updateCameraSelection()
        
        // Setup video processing pipeline.
        self.setupVideoProcessing()
        
        // Setup camera preview.
        self.setupCameraPreview()
        
        // Initialize the face detector.
        let options:[AnyHashable: Any] = [GMVDetectorFaceMinSize : NSNumber(value: 0.3),
                                          GMVDetectorFaceTrackingEnabled : NSNumber(value: true),
                                          GMVDetectorFaceLandmarkType : NSNumber(value:1 << 1),
                                          GMVDetectorFaceClassificationType : NSNumber(value:1 << 1)]
        self.faceDetector = GMVDetector.init(ofType: GMVDetectorTypeFace, options: options)
        
    }
    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.previewLayer!.frame = self.view.layer.bounds
        self.previewLayer!.position = CGPoint(x:self.previewLayer!.frame.size.width/2 , y: self.previewLayer!.frame.size.height/2)
    }
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.isNavigationBarHidden = true
        self.step = 0
        self.labelAction.text = "Để khuôn mặt giữa màn hình"
        self.imgViewAction.image = UIImage(named: "liveness_head.png")
    }
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.captureSession?.startRunning()
    }
    override public func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.captureSession?.stopRunning()
    }
    @IBAction func clickFlipCamera(_ sender: Any) {
        self.isUseFrontCamera = !self.isUseFrontCamera!
        self.updateCameraSelection()
    }
    func createListAction(){
        var listActionForChoose = [Any]()
        let dictLeft:[String:String]  = ["status":"1","name":"Quay trái","imageName":"liveness_left.png", "type": "left"]
        listActionForChoose.append(dictLeft)
        let dictRight:[String:String]  = ["status":"1","name":"Quay phải","imageName":"liveness_right.png", "type": "right"]
        listActionForChoose.append(dictRight)
        let dictSmile:[String:String]  = ["status":"1","name":"Mỉm cười","imageName":"smile.png", "type": "smile"]
        listActionForChoose.append(dictSmile)
        let dictBlink:[String:String]  = ["status":"1","name":"Nhắm mắt","imageName":"liveness_eye.png", "type": "eyeClose"]
        listActionForChoose.append(dictBlink)
        let dictMouth:[String:String]  = ["status":"1","name":"Há miệng","imageName":"liveness_mouth.png", "type": "openMouth"]
        listActionForChoose.append(dictMouth)
        let dictHeadUp:[String:String]  = ["status":"1","name":"Ngẩng lên","imageName":"liveness_head_up.png", "type": "headUp"]
        listActionForChoose.append(dictHeadUp)
        let dictHeadDown:[String:String]  = ["status":"1","name":"Cúi xuống","imageName":"liveness_head_down.png", "type": "headDown"]
        listActionForChoose.append(dictHeadDown)
        // create list action
        self.listAction = [Any]()
        let dictHead:[String:String]  = ["status":"0","name":"Để khuôn mặt giữa màn hình","imageName":"liveness_head.png", "type": "head"]
        self.listAction?.append(dictHead)
        // random 2 item
        for n in 1...2 {
            let index = Int.random(in: 0 ..< listActionForChoose.count)
            var item = listActionForChoose[index] as! [String:String]
            item["status"] = "\(n)"
            self.listAction?.append(item)
            listActionForChoose.remove(at: index)
        }
        let dictFace:[String:String]  = ["status":"3","name":"Nhìn thẳng màn hình","imageName":"liveness_head.png", "type": "head"]
        self.listAction?.append(dictFace)
    }
    func checkFaceCenter(nose:CGPoint, scaleAspectFull:CGFloat) -> Bool {
        let width = UIScreen.main.bounds.size.width
        let imageViewHeadCenter = CGPoint(x: self.imgViewHead.center.x*scaleAspectFull, y: self.imgViewHead.center.y*scaleAspectFull+width/10.0)
        let offset1 = width/2.0
        let offset2 = width/3.0
        if nose.x < imageViewHeadCenter.x+offset1 && nose.x > imageViewHeadCenter.x - offset1 && nose.y < imageViewHeadCenter.y + offset2 && nose.y > imageViewHeadCenter.y - offset2 {
            return true
        }
        return false
    }
    func checkHeadRight(nose:CGPoint, leftCheek: CGPoint, rightCheek:CGPoint) -> Bool {
        let distanceLeftCheckToNose = nose.x - leftCheek.x
        let distanceNoseToRightCheck = rightCheek.x - nose.x
        if distanceNoseToRightCheck < distanceLeftCheckToNose*1.0/3 {
            return true
        }
        return false
    }
    func checkHeadLeft(nose:CGPoint, leftCheek: CGPoint, rightCheek:CGPoint) -> Bool {
        let distanceLeftCheckToNose = nose.x - leftCheek.x
        let distanceNoseToRightCheck = rightCheek.x - nose.x
        if distanceLeftCheckToNose < distanceNoseToRightCheck*1.0/3 {
            return true
        }
        return false
    }
    func checkFace(nose:CGPoint, scaleAspectFull:CGFloat) -> Bool {
        let width = UIScreen.main.bounds.size.width
        let imageViewHeadCenter = CGPoint(x: self.imgViewHead.center.x*scaleAspectFull, y: self.imgViewHead.center.y*scaleAspectFull+width/10.0)
        let offset1 = CGFloat(25)
        let offset2 = CGFloat(25)
        if nose.x < imageViewHeadCenter.x + offset1 && nose.x > imageViewHeadCenter.x - offset1 && nose.y < imageViewHeadCenter.y + offset2 && nose.y > imageViewHeadCenter.y - offset2 {
            return true
        }
        return false
    }
    func checkFaceSize(faceRect:CGRect) -> Bool{
        let width = UIScreen.main.bounds.size.width
        if faceRect.size.width < width && faceRect.size.width > width*1.0/4 {
            return true
        }
        return false
    }
    func checkOpenMouth(nose: CGPoint, mouth: CGPoint, bottomMouth: CGPoint) -> Bool {
        let distance = bottomMouth.y - mouth.y
        let distanceNoseToMouth = mouth.y - nose.y
        if distance > distanceNoseToMouth*1.0/2 {
            return true
        }
        return false
    }
    func checkHeadUp(nose: CGPoint, leftEye: CGPoint, rightEye: CGPoint, mouth: CGPoint) -> Bool {
        var yEye = leftEye.y
        if leftEye.y > rightEye.y {
            yEye = rightEye.y
        }
        let disNoseToEye = nose.y - yEye
        let disMouthToNose = mouth.y - nose.y
        if disNoseToEye < disMouthToNose/3.0 {
            return true
        }
        return false
    }
    func checkHeadDown(nose: CGPoint, leftEye: CGPoint, rightEye: CGPoint, mouth: CGPoint) -> Bool {
        var yEye = leftEye.y
        if leftEye.y < rightEye.y {
            yEye = rightEye.y
        }
        let disNoseToEye = nose.y - yEye
        let disMouthToNose = mouth.y - nose.y
        if disMouthToNose < disNoseToEye {
            return true
        }
        return false
    }
    func checkStep(face:GMVFaceFeature, xScale:CGFloat, yScale:CGFloat, videoBox:CGRect, scaleAspectFull:CGFloat ){
        let dict = self.listAction![self.step] as! [String:String]
        self.labelAction.text = dict["name"]
        let podBundle = Bundle(for: CameraViewController.self)
        let bundleURL = podBundle.url(forResource: "VVNEkyc", withExtension: "bundle")
        let bundle = Bundle(url: bundleURL!)!
        self.imgViewAction.image = UIImage(named: dict["imageName"]!, in: bundle, compatibleWith: nil)
        var newPointNoseBase = CGPoint(x: 0, y: 0)
        var newPointLeftCheek = CGPoint(x: 0, y: 0)
        var newPointRightCheek = CGPoint(x: 0, y: 0)
        var newPointBottomMouth = CGPoint(x: 0, y: 0)
        var newPointMouth = CGPoint(x: 0, y: 0)
        var newPointLeftEye = CGPoint(x: 0, y: 0)
        var newPointRightEye = CGPoint(x: 0, y: 0)
        var isFaceCenter = false
        var isHeadRight = false
        var isHeadLeft = false
        var isFaceLookIn = false
        var isAllowFaceSize = false
        var isSmile = false
        var isBlink = false
        var isOpenMouth = false
        var isHeadUp = false
        var isHeadDown = false
        let newFace = CGRect(x: face.bounds.origin.y, y: face.bounds.origin.x, width: face.bounds.size.width, height: face.bounds.size.height)
        let faceRect = self.scaledRect(rect: newFace, xScale: xScale, yScale: yScale, offset: videoBox.origin)
        isAllowFaceSize = self.checkFaceSize(faceRect: faceRect)
        if face.hasNoseBasePosition {
            newPointNoseBase = CGPoint(x: face.noseBasePosition.y, y: face.noseBasePosition.x)
            newPointNoseBase = self.scaledPoint(point: newPointNoseBase, xScale: xScale, yScale: yScale, offset: videoBox.origin)
            isFaceCenter = self.checkFaceCenter(nose: newPointNoseBase, scaleAspectFull: scaleAspectFull)
            isFaceLookIn = self.checkFace(nose: newPointNoseBase, scaleAspectFull: scaleAspectFull)
        }
        // Cheeks
        if face.hasLeftCheekPosition {
            newPointLeftCheek = CGPoint(x: face.leftCheekPosition.y, y: face.leftCheekPosition.x)
            newPointLeftCheek = self.scaledPoint(point: newPointLeftCheek, xScale: xScale, yScale: yScale, offset: videoBox.origin)
        }
        if face.hasRightCheekPosition {
            newPointRightCheek = CGPoint(x: face.rightCheekPosition.y, y: face.rightCheekPosition.x)
            newPointRightCheek = self.scaledPoint(point: newPointRightCheek, xScale: xScale, yScale: yScale, offset: videoBox.origin)
        }
        if face.hasLeftCheekPosition && face.hasRightCheekPosition && face.hasNoseBasePosition {
            isHeadLeft = self.checkHeadLeft(nose: newPointNoseBase, leftCheek: newPointLeftCheek, rightCheek: newPointRightCheek)
            isHeadRight = self.checkHeadRight(nose: newPointNoseBase, leftCheek: newPointLeftCheek, rightCheek: newPointRightCheek)
        }
        if face.hasSmilingProbability {
            if face.smilingProbability > 0.7 {
                isSmile = true
            }
        }
        if face.hasLeftEyeOpenProbability && face.hasRightEyeOpenProbability {
            if face.rightEyeOpenProbability < 0.4 && face.leftEyeOpenProbability < 0.4 {
                isBlink = true
            }
        } else if face.hasLeftEyeOpenProbability == false && face.hasRightEyeOpenProbability == false {
            isBlink = true
        }
        if (face.hasBottomMouthPosition) {
            newPointBottomMouth = CGPoint(x: face.bottomMouthPosition.y, y: face.bottomMouthPosition.x)
            newPointBottomMouth = self.scaledPoint(point: newPointBottomMouth, xScale: xScale, yScale: yScale, offset: videoBox.origin)
        }
        if (face.hasMouthPosition) {
            newPointMouth = CGPoint(x: face.mouthPosition.y, y: face.mouthPosition.x)
            newPointMouth = self.scaledPoint(point: newPointMouth, xScale: xScale, yScale: yScale, offset: videoBox.origin)
        }
        if face.hasBottomMouthPosition && face.hasMouthPosition && face.hasNoseBasePosition {
            isOpenMouth = self.checkOpenMouth(nose: newPointNoseBase, mouth: newPointMouth, bottomMouth: newPointBottomMouth)
        }
        // Eyes
        if (face.hasLeftEyePosition) {
            newPointLeftEye = CGPoint(x: face.leftEyePosition.y, y: face.leftEyePosition.x)
            newPointLeftEye = self.scaledPoint(point: newPointLeftEye, xScale: xScale, yScale: yScale, offset: videoBox.origin)
        }
        if (face.hasRightEyePosition) {
            newPointRightEye = CGPoint(x: face.rightEyePosition.y, y: face.rightEyePosition.x)
            newPointRightEye = self.scaledPoint(point: newPointRightEye, xScale: xScale, yScale: yScale, offset: videoBox.origin)
        }
        if(face.hasNoseBasePosition && face.hasLeftEyePosition && face.hasRightEyePosition && face.hasMouthPosition){
            isHeadUp = self.checkHeadUp(nose: newPointNoseBase, leftEye: newPointLeftEye, rightEye: newPointRightEye, mouth: newPointMouth)
            isHeadDown = self.checkHeadDown(nose: newPointNoseBase, leftEye: newPointLeftEye, rightEye: newPointRightEye, mouth: newPointMouth)
        }
        switch self.step {
        case 0:
            if isFaceCenter && isAllowFaceSize {
                self.step = 1
            }
            break;
        case 1:
            if self.isNextStep(type: dict["type"]!, isFaceCenter: isFaceCenter, isHeadLeft: isHeadLeft, isHeadRight: isHeadRight, isSmile: isSmile, isBlink: isBlink, isOpenMouth: isOpenMouth, isHeadUp: isHeadUp, isHeadDown: isHeadDown) {
                self.step = 2
            }
            break;
        case 2:
            if self.isNextStep(type: dict["type"]!, isFaceCenter: isFaceCenter, isHeadLeft: isHeadLeft, isHeadRight: isHeadRight, isSmile: isSmile, isBlink: isBlink, isOpenMouth: isOpenMouth, isHeadUp: isHeadUp, isHeadDown: isHeadDown) {
                self.step = 3
                self.countDown = numCountDown
            }
            break;
        case 3:
            if(isFaceCenter && isFaceLookIn){
                self.countDown -= 1
                if self.countDown > 0 {
                    return
                }
                self.imageFace = self.imageFromSampleBuffer(sampleBuffer: self.buffer!)
                self.isChangeToResultView = true
                self.labelAction.text = "Success"
                self.imgViewAction.image = UIImage(named: "result_success.png")
                self.finishDetection()
            }
        default:
            break;
        }
    }
    func isNextStep(type: String, isFaceCenter: Bool, isHeadLeft: Bool, isHeadRight: Bool, isSmile: Bool, isBlink: Bool, isOpenMouth: Bool, isHeadUp: Bool, isHeadDown: Bool) -> Bool {
        if type == "left" {
            if isFaceCenter && isHeadLeft {
                return true
            }
        } else if type == "right" {
            if isFaceCenter && isHeadRight {
                return true
            }
        } else if type == "smile" {
            if isFaceCenter && isSmile {
                return true
            }
        } else if type == "eyeClose" {
            if isFaceCenter && isBlink {
                return true
            }
        } else if type == "openMouth" {
            if isFaceCenter && isOpenMouth {
                return true
            }
        } else if type == "headUp" {
            if isFaceCenter && isHeadUp {
                return true
            }
        } else if type == "headDown" {
            if isFaceCenter && isHeadDown {
                return true
            }
        }
        return false
    }

    func finishDetection() {
        self.delegate?.getFaceImageResult(imageFace: self.imageFace)
    }
    func scaledRect(rect:CGRect, xScale:CGFloat, yScale:CGFloat, offset: CGPoint) -> CGRect {
        let resultRect = CGRect(x: rect.origin.x * xScale+offset.x, y: rect.origin.y * yScale+offset.y, width: rect.size.width * xScale, height: rect.size.height * yScale)
        return resultRect
    }
    func scaledPoint(point:CGPoint, xScale:CGFloat, yScale:CGFloat, offset: CGPoint) -> CGPoint {
        let resultPoint = CGPoint(x: point.x * xScale + offset.x, y: point.y * yScale + offset.y)
        return resultPoint
    }
    public func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        if self.isChangeToResultView {
            DispatchQueue.main.sync {
                for featureView in self.overlayView.subviews {
                    featureView.removeFromSuperview()
                }
            }
            return
        }
        self.buffer = sampleBuffer;
        self.imageProcess = GMVUtility.sampleBufferTo32RGBA(sampleBuffer)
        let devicePosition = self.isUseFrontCamera! ? AVCaptureDevice.Position.front :
            AVCaptureDevice.Position.back
        
        if self.isUseFrontCamera! {
            // Establish the image orientation.
            let deviceOrientation = UIDevice.current.orientation
            let orientation = GMVUtility.imageOrientation(from: deviceOrientation, with: devicePosition, defaultDeviceOrientation: self.lastKnownDeviceOrientation)
            let options = [GMVDetectorImageOrientation : NSNumber(value:orientation.rawValue)]
            let faces = self.faceDetector?.features(in: self.imageProcess!, options: options)
            
            //print((faces?.count)! as! Int)
            let fdesc = CMSampleBufferGetFormatDescription(sampleBuffer)
            let clap = CMVideoFormatDescriptionGetCleanAperture(fdesc!, originIsAtTopLeft: false)
            let ratioClapToView = UIScreen.main.bounds.size.width*1.0/clap.size.width
            let scaleAspectFull = UIScreen.main.bounds.size.height*1.0/(UIScreen.main.bounds.size.height*ratioClapToView);
            let parentFrameSize = CGSize(width: UIScreen.main.bounds.size.width*scaleAspectFull, height: UIScreen.main.bounds.size.height*scaleAspectFull)
            let cameraRatio = clap.size.height / clap.size.width
            let viewRatio = parentFrameSize.width / parentFrameSize.height
            var xScale:CGFloat = 1.0
            var yScale:CGFloat = 1.0
            var videoBox = CGRect.init()
            if viewRatio > cameraRatio {
                videoBox.size.width = parentFrameSize.height * clap.size.width / clap.size.height;
                videoBox.size.height = parentFrameSize.height;
                videoBox.origin.x = (parentFrameSize.width - videoBox.size.width) / 2;
                videoBox.origin.y = (videoBox.size.height - parentFrameSize.height) / 2;
                
                xScale = videoBox.size.width / clap.size.width;
                yScale = videoBox.size.height / clap.size.height;
            } else {
                videoBox.size.width = parentFrameSize.width;
                videoBox.size.height = clap.size.width * (parentFrameSize.width / clap.size.height);
                videoBox.origin.x = (videoBox.size.width - parentFrameSize.width)/2;
                videoBox.origin.y = (parentFrameSize.height - videoBox.size.height)/2;
                xScale = videoBox.size.width / clap.size.height;
                yScale = videoBox.size.height / clap.size.width;
            }
            DispatchQueue.main.sync {
                for featureView in self.overlayView.subviews {
                    featureView.removeFromSuperview()
                }
                self.overlayView.frame = CGRect(x: 0, y: 0, width: parentFrameSize.width, height: parentFrameSize.height)
                self.overlayView.center = self.view.center
                // Display detected features in overlay.
                for face in faces! {
                    self.checkStep(face: face as! GMVFaceFeature, xScale: xScale, yScale: yScale, videoBox: videoBox, scaleAspectFull: scaleAspectFull)
                }
            }
        }
    }
    func cleanupVideoProcessing(){
        if (self.videoDataOutput != nil) {
            self.captureSession?.removeOutput(self.videoDataOutput!)
        }
        self.videoDataOutput = nil
    }
    func cleanupCaptureSession(){
        self.captureSession?.stopRunning()
        self.cleanupVideoProcessing()
        self.captureSession = nil
        self.previewLayer?.removeFromSuperlayer()
    }
    func updateCameraSelection(){
        self.captureSession?.beginConfiguration()
        
        let oldInputs = self.captureSession?.inputs
        for oldInput in oldInputs! {
            self.captureSession?.removeInput(oldInput)
        }
        
        let desiredPosition = self.isUseFrontCamera! ?
            AVCaptureDevice.Position.front : AVCaptureDevice.Position.back;
        let input = self.cameraForPosition(desiredPosition: desiredPosition)
        if input == nil {
            // Failed, restore old inputs
            for oldInput in oldInputs! {
                self.captureSession?.addInput(oldInput)
            }
        } else {
            let numInput = self.captureSession?.inputs.count
            print(numInput!)
            if numInput! > 0 {
                let oldInputs = self.captureSession?.inputs
                for oldInput in oldInputs! {
                    self.captureSession?.removeInput(oldInput)
                }
            }
            self.captureSession?.addInput(input!)
        }
        self.captureSession?.commitConfiguration()
    }
    func cameraForPosition(desiredPosition:AVCaptureDevice.Position) -> AVCaptureDeviceInput? {
        for device in AVCaptureDevice.devices(for: AVMediaType.video) {
            if (device.position == desiredPosition) {
                do {
                    let input = try AVCaptureDeviceInput.init(device: device)
                    if (self.captureSession?.canAddInput(input))! {
                        return input
                    }
                }catch {
                    print(error)
                }
                
            }
        }
        return nil
    }
    
    func setupVideoProcessing(){
        self.videoDataOutput = AVCaptureVideoDataOutput.init()
        let rgbOutputSettings = [kCVPixelBufferPixelFormatTypeKey:NSNumber(value:kCVPixelFormatType_32BGRA)]
        self.videoDataOutput?.videoSettings = rgbOutputSettings as [String : Any]
        
        if !(self.captureSession?.canAddOutput(self.videoDataOutput!))! {
            self.cleanupCaptureSession()
            return
        }
        self.videoDataOutput?.alwaysDiscardsLateVideoFrames = true
        self.videoDataOutput?.setSampleBufferDelegate(self, queue: self.videoDataOutputQueue)
        self.captureSession?.addOutput(self.videoDataOutput!)
    }
    func setupCameraPreview(){
        self.previewLayer = AVCaptureVideoPreviewLayer.init(session: self.captureSession!)
        self.previewLayer?.backgroundColor = UIColor.white.cgColor
        self.previewLayer?.videoGravity = .resizeAspectFill
        let rootLayer = self.placeHolder.layer
        rootLayer.masksToBounds = true
        self.previewLayer?.frame = rootLayer.bounds
        rootLayer.addSublayer(self.previewLayer!)
    }
}
